package main.java;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.scene.chart.XYChart;

import java.sql.ResultSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Created by Jarno on 7-1-2018.
 */
public class DataGrapper implements Runnable {
    DataTableDAO dataTableDAO = new DataTableDAO(new DBmanager());

    private final int Numb_Variables= 2;
    private XYChart.Series[] series = new XYChart.Series[Numb_Variables];
    private Vector<Number> boe = new Vector<Number>();
    private UI ui;
    private float[] float_array = new float[Numb_Variables];
    private long[] time_sent= new long[2];


    private void getDataDatabase(){
        //long[] TimeValues = ui.dateFromUntil();
        //dataTableDAO.buildData(TimeValues[0],TimeValues[1]);
    }

    public DataGrapper(UI o){
        this.ui = o;
    }

    @Override
    public void run() {

        time_sent[0] = System.currentTimeMillis();
        float_array[0] = 6;
        float_array[1] = 12;
        for(int i =0; i < Numb_Variables; i++){
            series[i] = new XYChart.Series();
        }
        for(int i =0; i <10; i++){
            boe.add(i*2);
        }

        series[0].setName("My portfolio");
        series[0].getData().add(new XYChart.Data(1, 23));
        series[0].getData().add(new XYChart.Data(2, 14));
        series[0].getData().add(new XYChart.Data(3, 15));
        series[0].getData().add(new XYChart.Data(4, 24));
        series[0].getData().add(new XYChart.Data(5, 34));
        series[0].getData().add(new XYChart.Data(6, 36));
        series[0].getData().add(new XYChart.Data(7, 22));
        series[0].getData().add(new XYChart.Data(8, 45));
        series[0].getData().add(new XYChart.Data(9, 43));
        series[0].getData().add(new XYChart.Data(10, 17));
        series[0].getData().add(new XYChart.Data(11, 29));
        series[0].getData().add(new XYChart.Data(11.5, 29));


        series[1].setName("My tester");
        series[1].getData().add(new XYChart.Data(1, 23));
        series[1].getData().add(new XYChart.Data(2, 15));
        series[1].getData().add(new XYChart.Data(3, 15));
        series[1].getData().add(new XYChart.Data(4, 8));
        series[1].getData().add(new XYChart. Data(5, 12));
        series[1].getData().add(new XYChart.Data(6, 47));

        while(true) {


//            ZonedDateTime dateTime = Instant.ofEpochMilli(System.currentTimeMillis())
//                    .atZone(ZoneId.of("Europe/Amsterdam"));
          //System.out.println(dateTime.toInstant().toEpochMilli());
            //System.out.println(dateTime.toString());
            ui.updateChart();
            getDataDatabase();
            try {
                Thread.sleep(1000);
                long[] temp = ui.dateFromUntil();


            } catch (Exception e) {
                System.out.println(e);
            }
        }


    }

    public long[] getFirstLastTime(){
        return time_sent;
    }

    public float[] getLastValues(){
        return float_array;
    }

    public XYChart.Series[] getSeries(){
        return series;
    }

}
