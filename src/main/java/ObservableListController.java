package main.java;

public class ObservableListController {

    private ObservableListModel model;
    private ObservableListView view;


    // Constructor that calls the init() methods.
    public ObservableListController (ObservableListModel model, ObservableListView view) {
        // Call the initialise methods.
        initModel(model);
        initView(view);
    }


    // Method that ensures the model (type: SeriesModel) is only set once in this class.
    private void initModel (ObservableListModel model) {

        if (this.model != null) {
            // There is already a model initialised.
            throw new IllegalStateException("The model of type 'ObservableListModel' can only be initialised once.");
        }
        // If not, initialise the model.
        this.model = model;


    }

    // Method that ensures the view (type: ObservableListView) is only set once in this class.
    private void initView (ObservableListView view) {
        if (this.view != null) {
            // There is already a view initialised.
            throw new IllegalStateException("The view of type 'ObservableListView' can only be initialised once.");
        }
        // If not, initialise the view.
        this.view = view;
    }



}





// Inner class for updating data to ObservableListModel from the database. //