package main.java;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.event.ActionListener;

public class SeriesView {

    private SeriesModel model;
    private SeriesController controller;
    // Object that will contain a reference to the primaryStage of the main Application class.
    private Stage primaryStage;

    // Create two axes for the LineChart.
    private final NumberAxis X_axis = new NumberAxis();
    private final NumberAxis Y_axis = new NumberAxis();
    // Create a LineChart.
    private final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(X_axis, Y_axis);
    // Multiple Series for the LineChart.
    private XYChart.Series<Number, Number> dataSeriesTemperature;

    // AnchorPane that will act as the root Pane.
    private AnchorPane lineChartPane; //
    // Scene that will be put on the Stage.
    private Scene lineChartScene;

    private Button showMainSceneButton;


    public SeriesView (SeriesModel model, SeriesController controller, Stage primaryStage) {

        this.model = model;

        this.controller = controller;

        this.primaryStage = primaryStage;

        initSeries();

        initLineChart();

        initButton();

        initScene();

    }


    //
    private void initSeries () {


    }

    private void initLineChart () {

        // Set the Labels of the axes.
        X_axis.setLabel("Time");
        Y_axis.setLabel("Value");
        // Set the title of the LineChart.
        lineChart.setTitle("Tiger's LoPi data");
        // Positioning of the LineChart in the Scene.
        lineChart.setPrefSize(720.00, 310.00);
        lineChart.setMinWidth(720.00);
        lineChart.setMinHeight(310.00);

        //lineChart.getData().add(model.);
    }

    private void initButton () {

        showMainSceneButton = new Button();

        showMainSceneButton.setText("Go to the main view.");
        showMainSceneButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Printing action to the console.
                System.out.println("Moving to the main view.");

                primaryStage.setTitle("Tiger's Weatherstation");
                primaryStage.setResizable(true);
                //primaryStage.setScene(observableListView.getMainScene()); // argument for method setScene() : Scene object of the SeriesView class.
            }
        });
        //showMainSceneButton.setOnAction(ser);
    }

    private void initScene () {

        /* LineChart Scene */

        // Create an VBox and set the properties of it.
        VBox lineChartVBox = new VBox();
        lineChartVBox.setSpacing(10.00);
        lineChartVBox.setPadding(new Insets(10.00, 0.00, 0.00, 10.00));
        lineChartVBox.getChildren().add(lineChart);   // Add a default graph here. Graph should later be changed by buttons. //

        // Create an HBox and set the properties of it.
        HBox lineChartButtonHBox = new HBox();
        lineChartButtonHBox.setPadding(new Insets(0.00, 10.00, 10.00, 10.00));
        lineChartButtonHBox.setSpacing(10.00);
        lineChartButtonHBox.getChildren().add(showMainSceneButton);

        lineChartPane = new AnchorPane();
        // Anchor the technical AnchorPane content to certain positions.
        AnchorPane.setTopAnchor(lineChartVBox, 10.00);
        AnchorPane.setLeftAnchor(lineChartVBox, 10.00);
        AnchorPane.setBottomAnchor(lineChartButtonHBox, 10.00);
        AnchorPane.setLeftAnchor(lineChartButtonHBox, 30.00);

        // Add the VBox with the LineChart and the HBox with the button(s) to the AnchorPane.
        lineChartPane.getChildren().addAll(lineChartVBox, lineChartButtonHBox);

        lineChartScene = new Scene(lineChartPane, 760.00, 440.00);
    }


    public Scene getLineChartScene () {
        return lineChartScene;
    }

}
