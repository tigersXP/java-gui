package main.java;

public class SeriesController {

    private SeriesModel model;
    private SeriesView view;
    private ObservableListView observableListView;


    // Constructor that calls the init() methods.
    public SeriesController (SeriesModel model, SeriesView view, ObservableListView observableListView) {
        // Call the initialise methods.
        initModel(model);
        initView(view);
        this.observableListView = observableListView; //
    }


    // Method that ensures the model (type: SeriesModel) is only set once in this class.
    private void initModel (SeriesModel model) {

        if (this.model != null) {
            // There is already a model initialised.
            throw new IllegalStateException("The model of type 'SeriesModel' can only be initialised once.");
        }
        // If not, initialise the model.
        this.model = model;


    }

    // Method that ensures the view (type: SeriesView) is only set once in this class.
    private void initView (SeriesView view) {
        if (this.view != null) {
            // There is already a view initialised.
            throw new IllegalStateException("The view of type 'SeriesView' can only be initialised once.");
        }
        // If not, initialise the view.
        this.view = view;
    }

    //

}


class ViewChangeListener {


}


