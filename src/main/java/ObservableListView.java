package main.java;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.Date;

public class ObservableListView {

    private ObservableListModel model;
    private SeriesView seriesView;
    // Object that will contain a reference to the primaryStage of the main Application class.
    private Stage primaryStage;

    // TableColumns for the TableView.
    private TableColumn<Data, Date> dateColumn;
    private TableColumn<Data, Float> temperatureColumn;
    private TableColumn<Data, Float> humidityColumn;
    private TableColumn<Data, Float> pressureColumn;
    private TableColumn<Data, Float> luminosityColumn;
    // TableView to show on the main view.
    private TableView<Data> mainTableView;

    // AnchorPane that will act as the root Pane.
    private AnchorPane mainPane; //
    // Label for the TableView.
    private Label mainLabel; //
    // Scene that will be put on the Stage.
    private Scene mainScene;

    // Button for switching to the technical LineChart Scene.
    private Button showLineChartScene;


    // Constructor that takes the primaryStage (of class: WeatherstationApplication) as parameter.
    // Calls the initialise methods.
    public ObservableListView (ObservableListModel model, Stage primaryStage) {

        this.model = model;

        //this.seriesView = seriesView;

        this.primaryStage = primaryStage;

        initColumns();

        initTableView();

        initButton();

        initScene();
    }


    private void initColumns () {

        // Initialise the TableColumn objects.
        dateColumn = new TableColumn<Data, Date>("Date / Time");
        temperatureColumn = new TableColumn<Data, Float>("Temperature (°C)");
        humidityColumn = new TableColumn<Data, Float>("Humidity (%)");
        pressureColumn = new TableColumn<Data, Float>("Pressure (mBar)");
        luminosityColumn = new TableColumn<Data, Float>("Luminosity (Lux)");

        // PropertyValueFactory objects as useful references.
        dateColumn.setCellValueFactory(new PropertyValueFactory<Data, Date>("date"));
        temperatureColumn.setCellValueFactory(new PropertyValueFactory<Data, Float>("temperature"));
        humidityColumn.setCellValueFactory(new PropertyValueFactory<Data, Float>("humidity"));
        pressureColumn.setCellValueFactory(new PropertyValueFactory<Data, Float>("pressure"));
        luminosityColumn.setCellValueFactory(new PropertyValueFactory<Data, Float>("luminosity"));

        // By default, the user can reorder the 4 data columns. Date/time cannot be reordered.
        dateColumn.setReorderable(false);
        temperatureColumn.setReorderable(true);
        humidityColumn.setReorderable(true);
        pressureColumn.setReorderable(true);
        luminosityColumn.setReorderable(true);

        // The TableColumns of the TableView cannot be resized.
        dateColumn.setResizable(false);
        temperatureColumn.setResizable(false);
        humidityColumn.setResizable(false);
        pressureColumn.setResizable(false);
        luminosityColumn.setResizable(false);
        // Manually set the width of the columns. There will still be some leftover empty column field.
        dateColumn.setMinWidth(185.00);
        temperatureColumn.setMinWidth(170.00);
        humidityColumn.setMinWidth(120.00);
        pressureColumn.setMinWidth(120.00);
        luminosityColumn.setMinWidth(120.00);
    }

    private void initTableView () {

        // Initialise object.
        mainTableView = new TableView<Data>();

        mainTableView.setEditable(true);
        // CONSTRAINED_RESIZE_POLICY only applies if ALL of the TableColumn's
        // in the TableView are resizable (setResizable = true).
        // Thus, this will only affect the horizontal row fields.
        // There will still be some extra space in the upper row of columns.
        mainTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        mainTableView.setMinWidth(720.00);
        mainTableView.setPrefSize(720.00, 310.00);

        // Add the columns to the TableView.
        // Arrays.asList to prevent type-safety violations.
        mainTableView.getColumns().addAll(Arrays.asList(dateColumn, temperatureColumn, humidityColumn, pressureColumn, luminosityColumn));
        /* OR, add the columns individually.
        // mainTableView.getColumns().add(dateColumn);
        // mainTableView.getColumns().add(temperatureColumn);
        // mainTableView.getColumns().add(humidityColumn);
        // mainTableView.getColumns().add(pressureColumn);
        // mainTableView.getColumns().add(luminosityColumn);
        */

        // Using the ObservableListModel object to access the method to get the data to put in the TableView.
        mainTableView.setItems(model.getDataMainView());
    }

    private void initButton () {

        showLineChartScene = new Button();

        showLineChartScene.setText("Show technical view.");
        showLineChartScene.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                // Printing action to the console.
                System.out.println("Moving to the technical view.");

                primaryStage.setTitle("Technical window : LineChart");
                primaryStage.setResizable(true);
                primaryStage.setScene(seriesView.getLineChartScene()); // argument for method setScene() : Scene object of the SeriesView class.
            }
        });
    }

    private void initScene () {

        /* Main Scene */

        mainLabel = new Label("Recent data results");
        mainLabel.setFont(new Font("Arial", 25));

        // Create an VBox and set the properties of it.
        VBox mainVBox = new VBox();
        mainVBox.setSpacing(10.00);
        mainVBox.setPadding(new Insets(10.00, 0.00 , 0.00 ,10.00));
        mainVBox.getChildren().addAll(mainLabel, mainTableView);

        // Create an HBox and set the properties of it.
        HBox mainButtonHBox = new HBox();
        mainButtonHBox.setPadding(new Insets(0.00, 10.00, 10.00, 10.00));
        mainButtonHBox.setSpacing(10.00);
        mainButtonHBox.getChildren().add(showLineChartScene);

        mainPane = new AnchorPane();
        // Anchor the main AnchorPane content to certain positions.
        AnchorPane.setTopAnchor(mainVBox, 10.00);
        AnchorPane.setLeftAnchor(mainVBox, 10.00);
        AnchorPane.setBottomAnchor(mainButtonHBox, 10.00);
        AnchorPane.setLeftAnchor(mainButtonHBox, 30.00);
        // Add the VBox with the TableView and the HBox with the button(s) to the main AnchorPane.
        mainPane.getChildren().addAll(mainVBox, mainButtonHBox);

        // Finally, create a Scene with mainPane as root Pane.
        mainScene  = new Scene(mainPane, 760.00, 440.00);
    }


    public TableView<Data> getMainTableView () {
        return mainTableView;
    }

    // Optional method. //
    public void updateMainTableView (TableView<Data> newMainTableView) {
        mainTableView = newMainTableView;
    }

    public Scene getMainScene () {
        return mainScene;
    }



}
