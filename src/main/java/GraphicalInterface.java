package main.java;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.CountDownLatch;


public class GraphicalInterface extends Application {

    public static void main(String[] args) {
        launch(args);
    }


    // Create a Stage to show and put the different Scenes on.
    private Stage primaryStageInstance;


    //Database connection
    DataTableDAO dataTableDAO;
    ResultSet resultSet;

    /* Main View */

    // Create a TableView to show on the main view.
    private TableView<Data> mainTableView = new TableView<Data>();
    // Create an ObservableList for storing retrieved data from the database.
    // This ObservableList will be put in the main TableView.
    private ObservableList<Data> data = FXCollections.observableArrayList();

    // Create an AnchorPane object, which will act as the root pane for the main Scene.
    private AnchorPane mainPane = new AnchorPane();
    // Create a Scene.
    // The Scene 'mainScene' functions as the main scene which will be shown at first and contains a TableView with the most recent data.
    private Scene mainScene = new Scene(mainPane, 760.00, 440.00);


    /* Technical View */

    // Variable for the amount of results to display in the main TableView.
    private int amountLastResults = 5;

    // Defining the axes of the LineChart.
    private final CategoryAxis lineChart_xAxis = new CategoryAxis();
    private final NumberAxis lineChart_yAxis = new NumberAxis();
    // Create the LineChart.
    private final LineChart<String, Number> lineChart = new LineChart<String, Number>(lineChart_xAxis, lineChart_yAxis);
    // Define an ArrayList that contains Series for the LineChart.
    private ArrayList<XYChart.Series<String, Number>> list;

    // Create an AnchorPane object, which will act as the root pane for the LineChart Scene.
    private AnchorPane lineChartPane = new AnchorPane();
    // Create a second Scene.
    // The 'lineChartScene' functions as the second scene which will show a LineChart.
    private Scene lineChartScene = new Scene(lineChartPane, 760.00, 850.00);

    //
    private long minimumRangeValue = 00l;
    private long maximumRangeValue = 00L; // call a method to get the maximum range value.
    // Two sliders for specifying the range in time of the data shown in the LineChart.
    private Slider minimumSlider = new Slider();
    private Slider maximumSlider = new Slider();
    // Labels for the respective sliders.
    private Label minimumSliderLabel = new Label();
    private Label maximumSliderLabel = new Label();

    private double first_value=0;
    // Start() method which will be called by the launch() method in the main() function.
    @Override
    public void start(Stage primaryStage) {

        primaryStageInstance = primaryStage;


        // Set the title of the stage of the JavaFX program.
        primaryStage.setTitle("Tiger's weather application");


        // Two buttons, each respectively for the main Scene and the technical Scene.
        // Used for switching between the two scenes.
        Button showLineChartSceneButton = new Button();
        showLineChartSceneButton.setText("Show technical view.");
        showLineChartSceneButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("Switching to the LineChart Scene.");
                primaryStage.setScene(lineChartScene);
                primaryStage.setResizable(true);
                primaryStage.setTitle("Technical view: LineChart");
            }
        });

        Button showMainSceneButton = new Button();
        showMainSceneButton.setText("Show main view.");
        showMainSceneButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("Going back to the main Scene.");
                primaryStage.setScene(mainScene);
                // NOTE: switching to the main Scene causes Stage resizability to be disabled!
                primaryStage.setResizable(false);
                primaryStage.setTitle("Tiger's weather application");
            }
        });



        /* Main TableView */

        // Edit the properties of the main TableView.

        mainTableView.setEditable(true);
        // CONSTRAINED_RESIZE_POLICY only applies if ALL of the TableColumn's
        // in the TableView are resizable (setResizable = true).
        // Thus, this will only affect the horizontal row fields.
        // There will still be some extra space in the upper row of columns.
        mainTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        mainTableView.setMinWidth(720.00);
        mainTableView.setPrefSize(720.00, 310.00);


        // Label for the TableView 'mainTableView'.
        final Label titleLabel = new Label("Recent data results");
        titleLabel.setFont(new Font("Arial", 25));


        // Create TableColumns for all the different types of data gathered by the LoPi, expressed in types Data (class), Date (Java-class) and Float.
        TableColumn<Data, Date> dateColumn = new TableColumn<Data, Date>("Date / Time");
        dateColumn.setCellValueFactory(new PropertyValueFactory<Data, Date>("date"));

        TableColumn<Data, Float> temperatureColumn = new TableColumn<Data, Float>("Temperature (°C)");
        temperatureColumn.setCellValueFactory(new PropertyValueFactory<Data, Float>("temperature"));

        TableColumn<Data, Float> humidityColumn = new TableColumn<Data, Float>("Humidity (%)");
        humidityColumn.setCellValueFactory(new PropertyValueFactory<Data, Float>("humidity"));

        TableColumn<Data, Float> pressureColumn = new TableColumn<Data, Float>("Pressure (Pa)");
        pressureColumn.setCellValueFactory(new PropertyValueFactory<Data, Float>("pressure"));

        TableColumn<Data, Float> luminosityColumn = new TableColumn<Data, Float>("Luminosity (lx)");
        luminosityColumn.setCellValueFactory(new PropertyValueFactory<Data, Float>("luminosity"));


        // Fill the TableView with the ObservableList contents.
        mainTableView.setItems(data);

        // Add the columns to the TableView.
        // Arrays.asList to prevent type-safety violations.
        mainTableView.getColumns().addAll(Arrays.asList(dateColumn, temperatureColumn, humidityColumn, pressureColumn, luminosityColumn));
        /* OR, add the columns individually.
        // mainTableView.getColumns().add(dateColumn);
        // mainTableView.getColumns().add(temperatureColumn);
        // mainTableView.getColumns().add(humidityColumn);
        // mainTableView.getColumns().add(pressureColumn);
        // mainTableView.getColumns().add(luminosityColumn);
        // mainTableView.getColumns().add(altitudeColumn);
        */

        // By default, the user can reorder the 5 data columns. Date/time cannot be reordered.
        dateColumn.setReorderable(false);
        temperatureColumn.setReorderable(true);
        humidityColumn.setReorderable(true);
        pressureColumn.setReorderable(true);
        luminosityColumn.setReorderable(true);
        // The TableView columns cannot be resized.
        dateColumn.setResizable(false);
        temperatureColumn.setResizable(false);
        humidityColumn.setResizable(false);
        pressureColumn.setResizable(false);
        luminosityColumn.setResizable(false);
        // Manually set the width of the columns. There will still be some leftover empty column field.
        dateColumn.setMinWidth(165.00);
        temperatureColumn.setMinWidth(150.00);
        humidityColumn.setMinWidth(100.00);
        pressureColumn.setMinWidth(100.00);
        luminosityColumn.setMinWidth(100.00);



        /* Technical View */

        // Set the Labels of the axes.
        lineChart_xAxis.setLabel("Time");
        lineChart_yAxis.setLabel("Value");
        // Set the title of the LineChart.
        lineChart.setTitle("Tiger's LoPi data");
        // Disable the animation of the LineChart.
        lineChart.setAnimated(false);
        // Disable the data points (dots) in the LineChart.
        lineChart.setCreateSymbols(false);

        //lineChart_yAxis.setTickUnit(1000.00);

        // Initialize the ArrayList that contains the Series objects.
        list = new ArrayList<>();
        // Add the Series objects to the ArrayList.
        list.add(new XYChart.Series<String,Number>());
        list.add(new XYChart.Series<String,Number>());
        list.add(new XYChart.Series<String,Number>());
        list.add(new XYChart.Series<String,Number>());
        // Set the correct names to the Series objects.
        list.get(0).setName("Temperature (°C)");
        list.get(1).setName("Humidity (%)");
        list.get(2).setName("Pressure (Pa)");
        list.get(3).setName("Luminosity (lx)");
        // Add the series to the LineChart.
        lineChart.getData().add(list.get(0));
        lineChart.getData().add(list.get(1));
        lineChart.getData().add(list.get(2));
        lineChart.getData().add(list.get(3));

        // Positioning of the LineChart in the Scene.
        lineChart.setPrefSize(720.00, 720.00);
        lineChart.setMinWidth(720.00);
        lineChart.setMinHeight(400.00);


        // Scaling feature for the LineChart.
        // Two Listeners listen for changes to the width and height, respectively, of the main stage.
        primaryStage.widthProperty().addListener((observable, oldValue, newValue) -> {
            // Calculate the scale of the change in width of the main stage.
            double scale = ( (Double) newValue / primaryStage.getMinWidth());

            lineChart.setPrefWidth(scale * lineChart.getMinWidth());
        });
        primaryStage.heightProperty().addListener((observable, oldValue, newValue) -> {
            // Calculate the scale of the change in height of the main stage.
            double scale = ( (Double) newValue / primaryStage.getMinHeight());

            lineChart.setPrefHeight(scale * lineChart.getMinHeight());
        });


        // Change the Slider settings.
        minimumSlider.setMin(0);
        //minimumSlider.setShowTickLabels(true);
        //minimumSlider.setShowTickMarks(true);
        maximumSlider.setMin(0);
       // maximumSlider.setShowTickLabels(true);
        //maximumSlider.setShowTickMarks(true);
        minimumSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(minimumSlider.getValue() >= maximumSlider.getValue()){
                    maximumSlider.setValue(minimumSlider.getValue() + 1);
                }
                minimumSliderLabel.setText(new Date((long)minimumSlider.getValue()).toString());
                updateChart();
            }
        });

        maximumSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(minimumSlider.getValue() >= maximumSlider.getValue()){
                    maximumSlider.setValue(minimumSlider.getValue() + 1);
                }
                maximumSliderLabel.setText(new Date((long)maximumSlider.getValue()).toString());
                updateChart();
            }
        });


        /* Main Scene */

        // Create a VBox and set the properties of it.
        VBox mainTableVBox = new VBox();
        mainTableVBox.setSpacing(10.00);
        mainTableVBox.setPadding(new Insets(10.00, 0.00 , 0.00 ,10.00));
        mainTableVBox.getChildren().addAll(titleLabel, mainTableView);

        // Create an HBox and set the properties of it.
        HBox mainButtonHBox = new HBox();
        mainButtonHBox.setPadding(new Insets(0.00, 10.00, 10.00, 10.00));
        mainButtonHBox.setSpacing(10.00);
        mainButtonHBox.getChildren().add(showLineChartSceneButton);

        // Anchor the main AnchorPane content to certain positions.
        AnchorPane.setTopAnchor(mainTableVBox, 10.00);
        AnchorPane.setLeftAnchor(mainTableVBox, 10.00);
        AnchorPane.setBottomAnchor(mainButtonHBox, 10.00);
        AnchorPane.setLeftAnchor(mainButtonHBox, 30.00);

        // Add the VBox with the TableView and the HBox with the button(s) to the main AnchorPane.
        mainPane.getChildren().addAll(mainTableVBox, mainButtonHBox);


        /* Technical Scene */

        // Create an VBox and set the properties of it.
        VBox lineChartVBox = new VBox();
        lineChartVBox.setSpacing(10.00);
        lineChartVBox.setPadding(new Insets(10.00, 0.00, 0.00, 10.00));
        lineChartVBox.getChildren().add(lineChart);

        // Create an HBox and set the properties of it.
        HBox lineChartButtonHBox = new HBox();
        lineChartButtonHBox.setPadding(new Insets(0.00, 10.00, 10.00, 10.00));
        lineChartButtonHBox.setSpacing(75.00);
        lineChartButtonHBox.getChildren().add(showMainSceneButton);
        lineChartButtonHBox.getChildren().add(minimumSlider);
        lineChartButtonHBox.getChildren().add(maximumSlider);
        lineChartButtonHBox.getChildren().add(minimumSliderLabel);
        lineChartButtonHBox.getChildren().add(maximumSliderLabel);

        // Anchor the technical AnchorPane content to certain positions.
        AnchorPane.setTopAnchor(lineChartVBox, 10.00);
        AnchorPane.setLeftAnchor(lineChartVBox, 10.00);
        AnchorPane.setBottomAnchor(lineChartButtonHBox, 10.00);
        AnchorPane.setLeftAnchor(lineChartButtonHBox, 30.00);

        // Add the VBox with the graph and the HBox with the button(s) to the technical AnchorPane.
        lineChartPane.getChildren().addAll(lineChartVBox, lineChartButtonHBox);



        /* Stage */

        // Set the main Scene on the stage and show it.
        primaryStage.setMinWidth(760.00);
        primaryStage.setMinHeight(460.00);
        primaryStage.setResizable(false);
        primaryStage.setScene(mainScene);
        primaryStage.show();



        // Close button request handler.
        primaryStage.setOnCloseRequest(event ->
        {
            Platform.exit();
            System.out.println("Terminating program.");
            // Terminate the running JVM.
            System.exit(0);
        }
        );

        dataTableDAO = new DataTableDAO(new DBmanager());

        new Thread(new DataGrapper()).start();
       // new Thread(new TestRunnable()).start();
        new Thread(new Updater()).start();

    }


    void updateChart(){
        try {

            resultSet.beforeFirst();
            double min = minimumSlider.getValue();
            double max = maximumSlider.getValue();
            Date date;
            for(int i =0; i < 4 ; i++){
                list.get(i).getData().clear();
            }
            //getting the data from the resultset and putting it in the respective series object
            while(resultSet.next()){
                if(min <=resultSet.getDouble(7)*1000 &&
                        max >= resultSet.getLong(7)*1000) {
                    //getting the time stamp
                    date = new Date(resultSet.getLong(7) * 1000);
                    //adding to the temp line
                    list.get(0).getData().add(new XYChart.Data<String, Number>(
                            date.toString(), resultSet.getFloat(3)));
                    //adding to the Humitity line
                    list.get(1).getData().add(new XYChart.Data<String, Number>(
                            date.toString(), resultSet.getFloat(4)));
                    //adding to the Pressure line
                    list.get(2).getData().add(new XYChart.Data<String, Number>(
                            date.toString(), resultSet.getFloat(5) / 1000));
                    //adding to the Luminosity line
                    list.get(3).getData().add(new XYChart.Data<String, Number>(
                            date.toString(), resultSet.getFloat(6)));
                }
            }
        }
        catch (Exception e){
            System.out.println("Graph Data inserted porperly");
        }
    }

    class Updater implements Runnable
    {
        @Override
        public void run() {

            try {
               resultSet = dataTableDAO.buildData();
               resultSet.next();
               first_value = resultSet.getDouble(7);


            }
            catch(Exception e){
                System.out.println("First value extracted correctly");
            }
            minimumSlider.setMin(first_value*1000);
            minimumSlider.setValue(first_value*1000);

            maximumSlider.setMin(first_value*1000);
            maximumSlider.setValue(System.currentTimeMillis());
            while (true) {
                Service<Void> service = new Service<Void>() {
                    @Override
                    protected Task<Void> createTask() {
                        return new Task<Void>() {
                            @Override
                            protected Void call() throws Exception {
                                //Background work
                                final CountDownLatch latch = new CountDownLatch(1);
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {

                                            if(minimumSlider.getValue() == minimumSlider.getMax()){
                                                minimumSlider.setMax(System.currentTimeMillis());
                                                minimumSlider.setValue(minimumSlider.getMax());
                                            }
                                            if(maximumSlider.getValue() == maximumSlider.getMax()){
                                                maximumSlider.setMax(System.currentTimeMillis());
                                                maximumSlider.setValue(maximumSlider.getMax());
                                            }
                                            minimumSlider.setMax(System.currentTimeMillis());
                                            maximumSlider.setMax(System.currentTimeMillis());
                                            minimumSliderLabel.setText(new Date((long)minimumSlider.getValue()).toString());
                                            maximumSliderLabel.setText(new Date((long)maximumSlider.getValue()).toString());
                                        } finally {
                                            latch.countDown();
                                        }
                                    }
                                });
                                latch.await();
                                //Keep with the background work
                                return null;
                            }
                        };
                    }
                };
                service.start();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    class DataGrapper implements Runnable
    {

        @Override
        public void run() {

            while (true) {
                Service<Void> service = new Service<Void>() {
                    @Override
                    protected Task<Void> createTask() {
                        return new Task<Void>() {
                            @Override
                            protected Void call() throws Exception {
                                //Background work
                                final CountDownLatch latch = new CountDownLatch(1);
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {

                                            updateChart();
                                            resultSet = dataTableDAO.buildData();

                                            try{
                                                resultSet.afterLast();
                                                int i =0;
                                                data.clear();
                                                //add the last retried results to the table view
                                                while(resultSet.previous() && i < amountLastResults){

                                                    i++;
                                                    //add a data observable list object
                                                    data.add(new Data(new Date(resultSet.getLong(7)*1000),resultSet.getFloat(3)
                                                            ,resultSet.getFloat(4),resultSet.getFloat(5)
                                                            ,resultSet.getFloat(6)));
                                                }

                                            }
                                            catch (Exception e){
                                                System.out.println("Table last results inserted correctly");
                                            }


                                        } finally {
                                            latch.countDown();
                                        }
                                    }
                                });
                                latch.await();
                                //Keep with the background work
                                return null;
                            }
                        };
                    }
                };
                service.start();
                try {
                    Thread.sleep(30000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}