package main.java;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;


/**
 * Created by Jarno on 7-1-2018.
 */
public class UI extends Application {

    LineChart<Number,Number> lineChart;
    final int numb_Variables = 2;
    boolean[] array = new boolean[numb_Variables];
    boolean[] array2 = new boolean[numb_Variables];
    XYChart.Series[] series = new XYChart.Series[numb_Variables];
    DataGrapper thread;
    NumberAxis xAxis;
    NumberAxis yAxis;
    GridPane gridpane;
    Slider slider;
    Slider slider2;


    public long[] dateFromUntil(){
        long[] temp = new long[2];
        temp[0] = (long)(slider.getValue()*1000);
        temp[1] = (long)(slider2.getValue()*1000);
        return temp;
    }

    public void updateChart() {

       Platform.runLater(new Runnable() {

           @Override
           public void run() {

               slider.setMin(thread.getFirstLastTime()[0]/1000);
               if(slider.getValue() < thread.getFirstLastTime()[0]/1000){
                   slider.setValue(thread.getFirstLastTime()[0]/1000);
               }

               slider.setMax(thread.getFirstLastTime()[1]/1000-1);

               slider2.setMin(thread.getFirstLastTime()[0]/1000);
               if(slider2.getValue() < slider.getValue()){
                   slider2.setValue(slider.getValue()+1);
               }
               if(slider2.getValue() == slider2.getMax()){
                   slider2.setMax(thread.getFirstLastTime()[1]/1000);
                   slider2.setValue(slider2.getMax());
               }
               slider2.setMax(thread.getFirstLastTime()[1]/1000);

               for(int i =0 ; i < numb_Variables; i++){
                        array2[i] = false;
                        lineChart.getData().remove(series[i]);
                }
               series = thread.getSeries();
               ChangeChart();
           }
       });
    }


    public void ChangeChart(){

        for(int i=0; i < numb_Variables; i++){
            if(array[i] && !array2[i]&& series[i] != null){
                array2[i] = true;
                lineChart.getData().add(series[i]);
            }

            else if(!array[i] && array2[i]){
                array2[i] = false;
                lineChart.getData().remove(series[i]);
            }
        }



    }
    @Override
    public void start(Stage primaryStage) throws Exception{

        thread = new DataGrapper(this);
        Thread t1 = new Thread(thread);
        t1.start();
        xAxis = new NumberAxis();
        yAxis = new NumberAxis();
        xAxis.setLabel("Number of Month");
        //creating the chart
        for(int i =0; i < numb_Variables; i++){
            series[i] = new XYChart.Series();
        }

        lineChart = new LineChart<Number,Number>(xAxis,yAxis);
        lineChart.setAnimated(false);

        lineChart.setTitle("Stock Monitoring, 2010");
        //defining a series
        array[0] = true;

        Button btn = new Button();
        btn.setText("OFF");

        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(btn.getText() == "ON"){
                    array[1] = false;
                    btn.setText("OFF");
                    ChangeChart();
                }
                else{
                    array[1] = true;
                    btn.setText("ON");
                    ChangeChart();
                }


            }
        });

        Label label = new Label(String.valueOf(System.currentTimeMillis()))   ;
        Label label2 = new Label(String.valueOf(System.currentTimeMillis()))   ;

        slider = new Slider(0,10,5);

        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                ZonedDateTime dateTime = Instant.ofEpochMilli((long)slider.getValue()*1000)
                        .atZone(ZoneId.of("Europe/Amsterdam"));
                label.textProperty().setValue(String.valueOf(dateTime.toString()));

            }
        });

        slider2 = new Slider(11,2000000000,600000000);
        slider2.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                ZonedDateTime dateTime = Instant.ofEpochMilli((long)slider2.getValue()*1000)
                        .atZone(ZoneId.of("Europe/Amsterdam"));
                label2.textProperty().setValue(String.valueOf(dateTime.toString()));
            }
        });

        gridpane = new GridPane();
        gridpane.setHgap(5);
        gridpane.setVgap(5);
        gridpane.add(lineChart,3,2);
        gridpane.add(label, 0,3);
        gridpane.add(label2, 1,3);
        gridpane.add(slider, 0,4);
        gridpane.add(slider2, 1,4);
        gridpane.add(btn,4,4);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(gridpane, 1024, 786));
        primaryStage.show();

    }
}
