package main.java;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

import java.sql.ResultSet;

public class SeriesModel {

    // Create a collection of these Series. //
    private XYChart.Series<Number, Number> dataSeriesDate;
    private XYChart.Series<Number, Number> dataSeriesTemperature;
    private XYChart.Series<Number, Number> dataSeriesHumidity;
    private XYChart.Series<Number, Number> dataSeriesPressure;
    private XYChart.Series<Number, Number> dataSeriesLuminosity;


    // Constructor
    public SeriesModel () {

        dataSeriesDate = new XYChart.Series<Number, Number> ();

        initDataTechnicalView();

    }


    /* Class functions go here. */ //

    // One of which should initially get some data entries from the database dependant on which range is selected. //
    private void initDataTechnicalView () {

    }


    public XYChart.Series<Number, Number> getDataSeriesDate () {
        return dataSeriesDate;
    }
}
