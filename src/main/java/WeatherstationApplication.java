package main.java;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

import static javafx.application.Application.launch;


public class WeatherstationApplication extends Application {

    private Stage primaryStage = new Stage();


    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start (Stage primaryStage) throws Exception {

        this.primaryStage = primaryStage;

        primaryStage.setTitle("Tiger's Weatherstation");


        // Create ObservableListModel, ObservableListView and ObservableListController objects here.
        // Calls the constructors of the respective classes.
        // The Stage 'primaryStage' will be passed to the constructor of ObservableListView as parameter.
        ObservableListModel observableListModel = new ObservableListModel();
        ObservableListView observableListView = new ObservableListView(observableListModel, primaryStage);

        SeriesModel seriesModel = new SeriesModel();
        //SeriesView seriesView = new SeriesView(seriesModel, primaryStage);

        //ObservableListController observableListController = new ObservableListController(observableListModel, observableListView, seriesView);
        //SeriesController seriesController = new SeriesController(seriesModel, seriesView, observableListView);


        // Configure Stage properties.
        primaryStage.setMinWidth(760.00);
        primaryStage.setMinHeight(460.00);
        primaryStage.setResizable(false);
        // By default, set the mainScene on the stage.
        primaryStage.setScene(observableListView.getMainScene());
        // Show the stage.
        primaryStage.show();


        // Close button request handler.
        primaryStage.setOnCloseRequest(event ->
                {
                    Platform.exit();
                    System.out.println("Terminating program.");
                    // Terminate the running JVM.
                    System.exit(0);
                }
        );
    }
    // Optional to catch exception(s).


    public Stage getPrimaryStage() {
        return primaryStage;
    }

    // More functions start on this line.

}
