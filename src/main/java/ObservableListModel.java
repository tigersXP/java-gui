package main.java;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.EventListener;

public class ObservableListModel {

    private ObservableList<Data> dataMainView;


    public ObservableListModel () {

        // perhaps move this line to the initDataMainView() method. //
        dataMainView = FXCollections.observableArrayList();

        initDataMainView();

    }


    /* Possible init() methods to call from the constructor. */ //

    // One of which should initially get the 5 most recent data entries from the database. //
    private void initDataMainView () {

    }


    public ObservableList<Data> getDataMainView () {
        return dataMainView;
    }

    // Optional method. //
    public void updateDataMainView (ObservableList<Data> newDataMainView) {
        dataMainView = newDataMainView;
    }

    // possible EventListener(s)? //

}
