package main.java;

import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Data {

    private final SimpleStringProperty date;
    private final SimpleFloatProperty temperature;
    private final SimpleFloatProperty humidity;
    private final SimpleFloatProperty pressure;
    private final SimpleFloatProperty luminosity;
    //private final SimpleFloatProperty altitude;

    protected Data (Date date, float temperature, float humidity, float pressure, float luminosity) {
        this.date = new SimpleStringProperty(createStringFromDate(date));
        this.temperature = new SimpleFloatProperty(temperature);
        this.humidity = new SimpleFloatProperty(humidity);
        this.pressure = new SimpleFloatProperty(pressure);
        this.luminosity = new SimpleFloatProperty(luminosity);
    }


    public String getDate() {
        return date.get();
    }
    // Using SimpleDateFormat class for testing purposes. // This functionality could also be used as final content.
    // Creates a Date class object that will be nicely formatte to a String.
    // This String will be returned.
    private String createStringFromDate(Date dateToFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E yyyy.MM.dd \n'at:' hh:mm:ss:SSS \na zzz");

        return simpleDateFormat.format(dateToFormat);
    }

    public float getTemperature() {
        return temperature.get();
    }

    public float getHumidity() {
        return humidity.get();
    }

    public float getPressure() {
        return pressure.get();
    }

    public float getLuminosity() {
        return luminosity.get();
    }

    /*public float getAltitude() {
        return altitude.get();
    }*/

}